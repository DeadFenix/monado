// Copyright 2020, Vis3r, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  NXTVR HMD driver.
 * @author Angel Mazo  <theanorak@protonmail.com>
 * @ingroup drv_nxtvr
 *
 * Special thanks to:
 * Jakob Bornecrantz
 */

#include "xrt/xrt_device.h"
#include "xrt/xrt_prober.h"
#include "xrt/xrt_defines.h"
#include "xrt/xrt_tracking.h"

#include "os/os_time.h"
#include "nxtvr_interface.h"
#include "hidapi/hidapi.h"

#include "math/m_api.h"
#include "math/m_imu_pre.h"
#include "math/m_imu_3dof.h"

#include "util/u_var.h"
#include "util/u_time.h"
#include "util/u_misc.h"
#include "util/u_debug.h"
#include "util/u_device.h"
#include "util/u_bitwise.h"

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>


#ifdef __cplusplus
extern "C" {
#endif

#define SIZE_BUFF 256
#define VID 0x1209
#define PID1 0x9D0F
#define PID2 0x6D0F
#define HID_REPORT_MOTION_ID 1
#define HID_INTERFACE 0
#define HID_REPORT_MOTION_SIZE 32
#define SAMPLES 50
#define NH_SPEW(nh, ...)                                                       \
	do {                                                                   \
		if (nh->print_spew) {                                          \
			fprintf(stderr, "%s - ", __func__);                    \
			fprintf(stderr, __VA_ARGS__);                          \
			fprintf(stderr, "\n");                                 \
		}                                                              \
	} while (false)

#define NH_DEBUG(nh, ...)                                                      \
	do {                                                                   \
		if (nh->print_debug) {                                         \
			fprintf(stderr, "%s - ", __func__);                    \
			fprintf(stderr, __VA_ARGS__);                          \
			fprintf(stderr, "\n");                                 \
		}                                                              \
	} while (false)

#define NH_ERROR(nh, ...)                                                      \
	do {                                                                   \
		fprintf(stderr, "%s - ", __func__);                            \
		fprintf(stderr, __VA_ARGS__);                                  \
		fprintf(stderr, "\n");                                         \
	} while (false)

struct nxtvr_parsed_sample
{
	struct xrt_vec3_i32 accel;
	struct xrt_vec3_i32 gyro;
};

struct filter_sample
{
	int32_t sum;
	int32_t averaged;
	int32_t readings[SAMPLES];
};

struct nxtvr_filter_sample
{
	struct filter_sample filter[6];
	int index;
};

struct nxt_hmd
{
	struct xrt_device base;
	struct nxtvr_parsed_sample sampleData;
	struct nxtvr_filter_sample sampleFilter;
	hid_device *hid;
	struct
	{
		//! Device time.
		uint64_t device_time;

		//! Pre filter for the IMU.
		struct m_imu_pre_filter pre_filter;

		struct m_imu_3dof fusion;
	};

	struct
	{
		bool last;
	} gui;
	bool print_debug;
	bool print_spew;
};

struct xrt_device *
nxtvr_device_create(struct hid_device_info *hmd_info,
                    struct xrt_prober *xp,
                    bool print_spew,
                    bool print_debug);

#ifdef __cplusplus
}
#endif
