// Copyright 2020, Vis3r, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  NXTVR HMD driver, based upon the psvr and arduino driver.
 * @author Angel Mazo (aka Anorak) <theanorak@protonmail.com>
 * @ingroup drv_nxtvr
 *
 * Special thanks to:
 * Jakob Bornecrantz
 */

#include "nxtvr_device.h"
#include <stdint.h>

static inline struct nxt_hmd *
nxt_hmd(struct xrt_device *dev)
{
	return (struct nxt_hmd *)dev;
}

static void
nxtvr_hmd_destroy(struct xrt_device *dev)
{
	struct nxt_hmd *nh = nxt_hmd(dev);
	u_var_remove_root(nh);
	u_device_free(&nh->base);
}

inline static uint8_t
read8(const unsigned char **buffer)
{
	uint8_t ret = **buffer;
	*buffer += 1;
	return ret;
}

inline static int32_t
read32(const unsigned char **buffer)
{
	int32_t ret = **buffer | (*(*buffer + 1) << 8) |
	              (*(*buffer + 2) << 16) | (*(*buffer + 3) << 24);
	*buffer += 4;
	return ret;
}


static void
nxtvr_device_filter_data(struct nxt_hmd *nh, const int32_t *sample)
{
    /*
     * This is a simple moving average algorithm, it filters the data from the 
     * IMU, in a method to cancel any kind on noise on it.
    */
	for (int i = 0; i < 6; i++) {
		nh->sampleFilter.filter[i].sum -=
		    nh->sampleFilter.filter[i].readings[nh->sampleFilter.index];
		nh->sampleFilter.filter[i].readings[nh->sampleFilter.index] =
		    sample[i];
		nh->sampleFilter.filter[i].sum += sample[i];
		nh->sampleFilter.index = (nh->sampleFilter.index + 1) % SAMPLES;
		nh->sampleFilter.filter[i].averaged =
		    nh->sampleFilter.filter[i].sum / SAMPLES;
	}
	nh->sampleData.accel.x = nh->sampleFilter.filter[0].averaged;
	nh->sampleData.accel.z = nh->sampleFilter.filter[1].averaged;
	nh->sampleData.accel.y = nh->sampleFilter.filter[2].averaged;
	nh->sampleData.gyro.x = nh->sampleFilter.filter[3].averaged;
	nh->sampleData.gyro.z = nh->sampleFilter.filter[4].averaged;
	nh->sampleData.gyro.y = nh->sampleFilter.filter[5].averaged;
}

static void
nxtvr_device_read_data(struct nxt_hmd *nh, const unsigned char *buff)
{
        int32_t sample[6];
	for (int i = 0; i < 6; i++) {
		sample[i] = read32(&buff);
	}
	sample[2] -= 16384;
	nxtvr_device_filter_data(nh, sample); 
}

static void
nxtvr_device_parse_input(struct nxt_hmd *nh,
                         int size,
                         const unsigned char *buff)
{
	if (size < 7) {
		NH_ERROR(nh, "%s Size id the report has to be at least 7!",
		         __func__);
	}
	uint8_t reportID = read8(&buff);

	switch (reportID) {
	    case HID_REPORT_MOTION_ID: 
		nxtvr_device_read_data(nh, buff); break;

	default: NH_ERROR(nh, "%s UNKNOWN REPORT ID!", __func__); break;
	}
}

static void
nxtvr_device_update_fusion(struct nxt_hmd *nh,
                           struct nxtvr_parsed_sample *sample,
                           timepoint_ns timestamp_ns,
                           time_duration_ns delta_ns)
{

	// TODO: quaternions are in zeros, cuz we have not set the bias an the
	// gain for the IMU. Seems like i would have to do all of the offset
	// calibration in the monado driver, rather than in the arduino side of
	// things.
	struct xrt_vec3 accel, gyro;
	m_imu_pre_filter_data(&nh->pre_filter, &sample->accel, &sample->gyro,
	                      &accel, &gyro);
	nh->device_time += (uint64_t)delta_ns / 10;
	m_imu_3dof_update(&nh->fusion, nh->device_time, &accel, &gyro);
	double delta_device_ms = (double)delta_ns / 1000.0;
	double delta_host_ms = (double)delta_ns / (1000.0 * 1000.0);
	NH_DEBUG(nh, "%+fms %+fms", delta_host_ms, delta_device_ms);
	NH_DEBUG(nh, "fusion sample %u (ax %d ay %d az %d) (gx %d gy %d gz %d)",
	         (int)timestamp_ns, sample->accel.x, sample->accel.y,
	         sample->accel.z, sample->gyro.x, sample->gyro.y,
	         sample->gyro.z);
	NH_DEBUG(nh, "\n");
}

static bool
nxtvr_device_read_one_packet(struct nxt_hmd *nh, unsigned char *buff, int size)
{
	while (true) {
		size = hid_read(nh->hid, buff, SIZE_BUFF);
		// if we got value of 0 or less, means that we can't read the
		// device
		if (size < 0) {
			fprintf(stderr, "Can't read the device %s\n", __func__);
			return false;
		}
		if (size == 0) {
			continue;
		}
		fprintf(stderr, "Got data!\n");
		fprintf(stderr, "Size is %i\n", size);
		// Parse the data we got.
		nxtvr_device_parse_input(nh, size, buff);
		return true;
	}
}

static void *
nxtvr_handle_sensor_msg(struct nxt_hmd *nh)
{
	unsigned char buff[SIZE_BUFF];
	timepoint_ns then_ns, now_ns;
	int size = -1;

	// wait for a package to sync up, it's discarded but that's okay.
	if (!nxtvr_device_read_one_packet(nh, buff, size)) {
		return NULL;
	}

	then_ns = os_monotonic_get_ns();
	if (nxtvr_device_read_one_packet(nh, buff, size)) {
		// As close to when we get a packet.
		now_ns = os_monotonic_get_ns();
		time_duration_ns delta_ns = now_ns - then_ns;
		then_ns = now_ns;
		// Process the parsed data.
		nxtvr_device_update_fusion(nh, &nh->sampleData, now_ns,
		                           delta_ns);
	}
	return NULL;
}

static void
nxtvr_get_fusion_pose(struct nxt_hmd *nh,
                      enum xrt_input_name name,
                      struct xrt_space_relation *out_relation)
{
	out_relation->pose.orientation = nh->fusion.rot;

	//! @todo assuming that orientation is actually currently tracked.
	out_relation->relation_flags = (enum xrt_space_relation_flags)(
	    XRT_SPACE_RELATION_ORIENTATION_VALID_BIT |
	    XRT_SPACE_RELATION_ORIENTATION_TRACKED_BIT);
}

static void
nxtvr_device_get_view_pose(struct xrt_device *xdev,
                           const struct xrt_vec3 *eye_relation,
                           uint32_t view_index,
                           struct xrt_pose *out_pose)
{
	struct xrt_pose pose = {{0.0f, 0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}};
	bool adjust = view_index == 0;

	pose.position.x = eye_relation->x / 2.0f;
	pose.position.y = eye_relation->y / 2.0f;
	pose.position.z = eye_relation->z / 2.0f;

	// Adjust for left/right while also making sure there aren't any -0.f.
	if (pose.position.x > 0.0f && adjust) {
		pose.position.x = -pose.position.x;
	}
	if (pose.position.y > 0.0f && adjust) {
		pose.position.y = -pose.position.y;
	}
	if (pose.position.z > 0.0f && adjust) {
		pose.position.z = -pose.position.z;
	}

	*out_pose = pose;
}

static void
nxtvr_device_destroy(struct xrt_device *dev)
{
	struct nxt_hmd *nh = nxt_hmd(dev);

	// Remove the variable tracking.
	u_var_remove_root(nh);

	// Destroy the fusion.
	m_imu_3dof_close(&nh->fusion);

	hid_close(nh->hid);

	nh->hid = NULL;

	nxtvr_hmd_destroy(dev);

	free(nh);
}

static void
nxtvr_device_update_inputs(struct xrt_device *dev)
{
	struct nxt_hmd *nh = nxt_hmd(dev);
	uint64_t now = os_monotonic_get_ns();
	nh->base.inputs[0].timestamp = now;
	nxtvr_handle_sensor_msg(nh);
}

static void
nxtvr_device_get_tracked_pose(struct xrt_device *xdev,
                              enum xrt_input_name name,
                              uint64_t at_timestamp_ns,
                              struct xrt_space_relation *out_relation)
{
	struct nxt_hmd *nh = nxt_hmd(xdev);

//	uint64_t now = os_monotonic_get_ns();

	(void)at_timestamp_ns;
	nxtvr_get_fusion_pose(nh, name, out_relation);
//	*out_relation_timestamp_ns = now;
}

static int
open_hid(struct nxt_hmd *nh,
         struct hid_device_info *dev_info,
         hid_device **out_dev)
{
	hid_device *dev = NULL;

	dev = hid_open_path(dev_info->path);
	if (dev == NULL) {
		NH_ERROR(nh, "Failed to open '%s'", dev_info->path);
		return -1;
	}

	int ret = hid_set_nonblocking(dev, 1);
	if (ret != 0) {
		NH_ERROR(nh, "Failed to set non-blocking on device");
		hid_close(dev);
		return -1;
	}
	*out_dev = dev;
	return 0;
}

struct xrt_device *
nxtvr_device_create(struct hid_device_info *hmd_info,
                    struct xrt_prober *xp,
                    bool print_spew,
                    bool print_debug)
{
	enum u_device_alloc_flags flags = (enum u_device_alloc_flags)(
	    U_DEVICE_ALLOC_HMD | U_DEVICE_ALLOC_TRACKING_NONE);

	struct nxt_hmd *nh = U_DEVICE_ALLOCATE(struct nxt_hmd, flags, 1, 0);

	nh->print_spew = print_spew;
	nh->print_debug = print_debug;
	nh->base.destroy = nxtvr_device_destroy;
	nh->base.update_inputs = nxtvr_device_update_inputs;
	nh->base.get_tracked_pose = nxtvr_device_get_tracked_pose;
	nh->base.get_view_pose = nxtvr_device_get_view_pose;
	nh->base.device_type = XRT_DEVICE_TYPE_HMD;
	nh->base.inputs[0].name = XRT_INPUT_GENERIC_HEAD_POSE;
	nh->base.name = XRT_DEVICE_GENERIC_HMD;

	struct u_device_simple_info info;
	info.display.w_pixels = 1920;
	info.display.h_pixels = 1080;
	info.display.w_meters = 0.13f;
	info.display.h_meters = 0.07f;
	info.lens_horizontal_separation_meters = 0.13f / 2.0f;
	info.lens_vertical_position_meters = 0.07f / 2.0f;
	info.views[0].fov = 85.0f * (M_PI / 180.0f);
	info.views[1].fov = 85.0f * (M_PI / 180.0f);

	m_imu_3dof_init(&nh->fusion, M_IMU_3DOF_USE_GRAVITY_DUR_300MS);

	float accel_ticks_to_float = (1.0f / 16384.0f) * 9.8f;
	float gyro_ticks_to_float = 1.0f / 131.0f;

	m_imu_pre_filter_init(&nh->pre_filter, accel_ticks_to_float,
	                      gyro_ticks_to_float);
//	m_imu_pre_filter_set_switch_x_and_y(&nh->pre_filter);

	int ret = open_hid(nh, hmd_info, &nh->hid);
	if (ret != 0) {
		NH_ERROR(nh, "Can't open hid!");
		nxtvr_device_destroy(&nh->base);
		return NULL;
	}
	if (!u_device_setup_split_side_by_side(&nh->base, &info)) {
		NH_ERROR(nh, "Failed to setup basic device info");
		nxtvr_device_destroy(&nh->base);
		return NULL;
	}

	u_var_add_root(nh, "NxtVR HMD", true);
	u_var_add_gui_header(nh, &nh->gui.last, "Last");
	u_var_add_ro_vec3_f32(nh, &nh->fusion.last.accel, "last.accel");
	u_var_add_ro_vec3_f32(nh, &nh->fusion.last.gyro, "last.gyro");

	NH_DEBUG(nh, "Created device!");
	NH_DEBUG(nh, "Created device");
	return &nh->base;
}
