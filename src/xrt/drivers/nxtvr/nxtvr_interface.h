// Copyright 2020, Vis3r, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  NXTVR HMD driver.
 * @author Angel Mazo  <theanorak@protonmail.com>
 * @ingroup drv_nxtvr
 *
 * Special thanks to:
 * Jakob Bornecrantz
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


struct xrt_auto_prober *
nxtvr_create_auto_prober(void);

#ifdef __cplusplus
}
#endif
